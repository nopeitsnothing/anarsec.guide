+++
title="Why Anarchists Shouldn't Have Phones"
date=2023-04-06

[taxonomies]
categories = ["Defensive"]
tags = ["mobile"]

[extra]
blogimage="/images/prison.jpg"
toc=true
dateedit=2023-05-10
a4="nophones-a4.pdf"
letter="nophones-letter.pdf"
+++

With effective [security culture and OPSEC](https://www.csrc.link/read/csrc-bulletin-1-en.html#header-a-base-to-stand-on-distinguishing-opsec-and-security-culture), the forces of repression wouldn't  know about our specific criminal activities, but they also wouldn't know about our lives, [relationships](https://www.csrc.link/threat-library/techniques/network-mapping.html), movement patterns, etc. This knowledge is a huge asset to help them narrow down suspects and execute targeted surveillance. The location of your phone is [tracked at all times](https://www.vice.com/en/article/m7vqkv/how-fbi-gets-phone-data-att-tmobile-verizon), and this data is harvested by private companies, enabling police to bypass laws requiring them to obtain a warrant. [Hardware identifiers and the subscription information](https://anonymousplanet.org/guide.html#your-imei-and-imsi-and-by-extension-your-phone-number) of the phone are logged by cell towers with every connection. Hacking services like [Pegasus](https://www.amnesty.org/en/latest/research/2021/07/forensic-methodology-report-how-to-catch-nso-groups-pegasus/) bring total phone compromise within the reach of even local law enforcement agencies, and are 'zero-click', meaning that success doesn't rely on you clicking a link or opening a file. 

<!-- more -->

On the flip side, after over 30 recent arsons went unsolved in a small city in France, [investigators complained](https://actforfree.noblogs.org/post/2022/04/17/grenoblefrance-these-saboteurs-of-the-ultra-left-have-been-elusive-for-five-years/) that "It is impossible to exploit telephone or vehicle registration data, since they operate without telephones or cars!" This article will map out some strategies for getting around any need you might have for a phone.

# Encryption and Geolocation

Some comrades approach the issues with smartphones by using flip phones or a landline to communicate with each other, but this approach leaves nothing obscured from the eyes of the State because nothing is [encrypted](/glossary/#encryption) - neither the content of your conversations nor who is speaking with whom. For example, in a [recent repressive operation](https://www.csrc.link/#quelques-premiers-elements-du-dossier-d-enquete-contre-ivan), police set up real-time geolocation of the suspect's phone, and made a list of everyone the suspect communicated with using unencrypted phone conversations over the course of a year. A brief biography was made for each contact.

It has become quite common for comrades to carry a cellphone around with them wherever they go, and in the contexts where people use flip phones, to make unencrypted calls to other anarchists. We should not make the jobs of police and intelligence agencies so easy; if the police don't have a phone to track, their only means of location tracking is an around-the-clock physical surveillance effort, which is resource-intensive and can be detected. The first step in such an effort at surveillance is to create a movement profile for the target, and cellphone geolocation history provides this in all-encompassing detail. Another primary goal of targeted surveillance is to map the target's social network in order to identify other targets. The only way to avoid providing this to our enemies on a silver platter is to exclusively use encrypted tools to communicate with other anarchists; these tools can help to prevent investigators from knowing who speaks with whom, or what they speak about.  

# Metadata Patterns

The normalization of constant connectedness in dominant society has led some anarchists to rightfully note that the [metadata](/glossary/#metadata) generated from phone activity is useful to investigators.  However, the conclusion that some draw from this insight, namely that we should ['never turn off the phone'](https://www.csrc.link/#never-turn-off-the-phone-a-new-approach-to-security-culture), leads us in the wrong direction entirely. Their logic is that if you step outside of normal metadata patterns, these moments become suspect, and if these moments align with when an action occurs, this could be used as evidence to link you to the crime or investigate you more closely. This is true, but the only conclusion to draw from this - which is not a total dead end, at least - is to minimize creating normal metadata patterns in the first place.

Our connections to the infrastructures of domination must remain sporadic and unpredictable if we are to retain any semblance of freedom and ability to strike the enemy. What if the reconnaissance required for an action requires an entire weekend away from electronic devices? Or let's start from the simple fact that phones must be left at home during an action - this only becomes the outlier to a pattern if phones otherwise accompany us wherever we go. With a normatively 'always connected' life, either of these metadata changes would stick out like a sore thumb, but this is not the case if you refuse to always plug yourself in.

# Do You Really Need a Phone? 

Whether or not you need a phone comes down to whether you need *synchronous* communication every moment of your life. [*Synchronous*](/glossary/#synchronous-communication) means when two or more parties communicate in real time, versus something [*asynchronous*](/glossary/#asynchronous-communication) like email, where messages are sent at different times. This 'need' has become normalized, but it is worth pushing back against within the anarchist space. [Anarchy can only be anti-industrial](https://theanarchistlibrary.org/library/bismuto-beyond-the-moment#toc1), and this requires that we learn to live without the conveniences sold to us by telecom companies: we ought to be able to live without being connected to the Internet at all times, without algorithmic real-time directions, and without an infinite flexibility that enables us to change plans at the last minute.

If you absolutely must use a phone, it should be as difficult as possible for an adversary to geotrack, intercept messages, or hack, which means using [GrapheneOS](/posts/grapheneos/). This is because *exclusively* using [encrypted communication](/posts/e2ee/) to communicate with other anarchists rules out flip phones and landlines, and GrapheneOS is the only smartphone option that has reasonable privacy and security. To avoid your movements being tracked, you must treat the smartphone like a landline and leave it at home when you are out of the house. Even if you use an anonymously acquired SIM, if this is linked to your identity in the future, the service provider can be retroactively queried for all geolocation data. If you use the phone as we recommend as a [Wi-Fi-only device](/posts/grapheneos/#what-is-grapheneos), and if you keep airplane mode enabled at all times, cell towers can't connect to the phone. Nevertheless, [malware](/glossary/#malware) compromise could still turn it into a audio recording device or log GPS history. Additionally, it's insufficient to only leave it at home when you are going to a demo or action because this pattern of behaviour then stands out as an outlier, serving as a hint that there is criminal activity happening in that time window.  

However, it's best to avoid using phones altogether. If it's only the comrades who are taking the biggest risks who are enacting these measures, they will stand out. Identical in principle to the black bloc tactic, the simple act of donning a mask will provide cover for anyone to act anonymously. Therefore, our proposal is that the parts of the anarchist space which have been swept up by dominant society's relationship to technology take several steps back to re-establish less intrusive baselines around phones. The strategies we will explain in the remainder of this article to live without phones rely on computers, where synchronous communication is possible but more limited, as your computer generally stays at home.

## Bureaucracy 

Many bureaucratic organizations make it difficult to not have a phone: healthcare, the post office, banking, etc. Since these communications do not need to be encrypted, you can use a [Voice over Internet Protocol (VoIP)](/glossary#voip-voice-over-internet-protocol) application (which allows you to make phone calls over the Internet rather than through cell towers). 

Any VoIP application option on a computer will be asynchronous because it won't ring when the computer is off - you rely on the voicemail feature to return missed calls. For example, a service like [jmp.chat](https://www.kicksecure.com/wiki/Mobile_Phone_Security#Phone_Number_Registration_Unlinked_to_SIM_Card) gives you a VoIP number that you can optionally pay for in Bitcoin, and you make calls through a XMPP (Jabber) client - [Cheogram](https://cheogram.com/) works well. 

Although typically more expensive then VoIP, a flip phone or landline also works well for making and receiving 'normal life' calls if you aren't going to be using it to speak with anarchists, and, in the case of the flip phone, leaving it at home. These have the advantage that you don't need a computer to be on to hear it ring.

A flip phone can be used for any [Two-Factor Authentication](/glossary/#two-factor-authentication-2fa) (2FA) that you require (when a service makes you receive a text message to log in), which do not always work with VoIP providers. If you only need a flip phone for 2FA, [online phone numbers](https://anonymousplanet.org/guide.html#online-phone-number) are another option. 

## Communication

Not having a phone will require changing how you socialize if you are [already caught in the net](https://theanarchistlibrary.org/library/return-fire-vol-4-supplement-caught-in-the-net). Being intentional about minimizing the mediation of screens in our relationships is a valuable goal in and of itself. 

Except in cases where it cannot be avoided (as in the case of a publication whose editors live in different regions from one another), organizing should not be mediated by technology. The dynamic by which, in some parts of the anarchist space, the entirety of how anarchists organize projects together has been reduced to a monoculture of Signal group chats (or worse) warrants a lot of criticism. This capture of organizing relationships by smartphone culture has given rise to a meeting that never ends, which is bad for a lot of anarchists' morale. It also means that our organizing is relatively easy to surveil. Only one phone in the group chat needs to be compromised with malware for access to all messages. 

That said, encrypted communication is useful for setting up real-life meet-ups where life and organizing actually takes place, or for projects that are shared with comrades across distances. See [Encrypted Messaging for Anarchists](/posts/e2ee/) for different options that are appropriate for an anarchist [threat model](/glossary/#threat-model).  

## Emergency Calls 

A passer-by on the street will often let you borrow their phone to make an urgent call. If the need arises in remote regions such as during a hiking trip, this is where using a flip phones would be a good fit. For receiving emergency calls, if you are not reachable from a computer as outlined above, we can drop by one another's houses or arrange for encrypted messaging check-ins ahead of time. What scenarios actually require being able to receive a call at any moment? If these actually exist in your life, you can organize to accommodate for them without projecting this urgency onto all areas and moments of your life.

## Directions

Buy a paper map of your region and bring it with you. For trips that are longer or where you will need directions, use [OpenStreetMap](https://www.openstreetmap.org/) to note them ahead of time. Wear a watch to be able to get where you are going on time.  

## Music and Podcasts 

They still make MP3 players! For a way cheaper price, you can play music and podcasts, but the device has neither GPS nor radio hardware. This does not mean they can't be used for geolocation. If your Wi-Fi is on, the approximate location of your MP3 player can be determined from the IP address.

# Appendix: Against the Smartphone

*From [Fernweh (#24)](https://fernweh.noblogs.org/texte/24-ausgabe/gegen-das-smartphone/)* 

It's always with us, it's always on, no matter where we are or what we're doing. It informs us about everything and everyone: what our friends are doing, when the next subway leaves and what the weather will be like tomorrow. It takes cares of us, wakes us up in the morning, reminds us of important appointments and always listens to us. It knows everything about us, when we go to bed, where we are and when, who we communicate with, who our best friends are, what music we listen to and what our hobbies are. And all it asks for is a little electricity now and then? 

When I stroll through an area or take the subway, I see it with almost everyone and no one can last more than a few seconds without frantically reaching for their pocket: the cellphone whipped out, a message sent, an email checked, a photo liked. It is put away again, a short break, and here we go again, skimming over today's news and looking at what all the friends are doing...

It's our companion when we're on the toilet, at work or at school, and it apparently helps to fight boredom while we're waiting or working, etc. Is this perhaps one of the reasons for the success of all these technological devices, that real life is so damn boring and monotonous that a screen of a few square centimeters is almost always more exciting than the world and the people around us? Is it like an addiction (people definitely have withdrawal symptoms...) or has it even become part of our bodies? Without it we no longer know how to orientate ourselves, and feel that something is missing? So it no longer just an aid or a toy but a part of us that also exerts a certain control over us, to which we adapt, for example by not leaving the house until the battery is fully charged? The smartphone as a first step in blurring the line between human and robot? 

When we see what technocrats of all kinds are prophesying (Google Glasses, implanted chips, etc.), it almost seems as if we are heading towards becoming Cyborgs, people with implanted smartphones that we control through our thoughts (until our thoughts themselves are eventually controlled). It is not surpising that the spokesmen of domination, the media, only show us the positive aspects of this development, but it is shocking that virtually no one questions this view. This is probably the wildest dream of any ruler: to be able to monitor everyone's thoughts and actions at all times and to be able to intervene immediately in the event of any disturbance. Completely controlled and monitored worker bees, who are allowed to have a little (virtual) fun as a reward while a few profit. 

With the vast amounts of data now so readily available from anyone and everyone at any time of day, social control and surveillance has also reached a whole new level. This now goes far beyond tapping cell phones or sifting through messages (like during the 2011 UK riots). By having access to an incredible amount of information, intelligence agencies are able to define a status that is "normal." They can tell which locations are "normal" for us, which contacts are "normal," etc. In short, they can establish immediately and in almost real time whether people are deviating from their "normal" behaviour. This gives enormous power to some people, which will be used whenever there is an opportunity to take advantage of this power (i.e. to surveil people). Technology is part of power, it emerges from it and needs it. It takes a world where people have extreme power in order to enable the production of something like the smartphone in the first place. All technology is a product of the current oppressive world, is part of it and will reinforce it.

Nothing is neutral in today's world. To date, everything that has been or is being developed serves both to extend control and to make money. Many of the innovations of the last decades (like GPS, nuclear power or the internet) even come directly from the military. Most of the time, these two aspects go hand in hand, but the "welfare of humanity" is surely not a motivation, and especially not if it is developed by the military. 

It is possible that by taking the example of architecture this better illustrates something as complex as technology: let's take an empty and disused prison, what should be done with this structure except tear it down? Its architecture alone, its walls, its watchtowers and its cells already contain the purpose of this building: to lock people up and destroy them psychologically. It would be impossible for me to live there, simply because the building already carries oppression within it. 

It is the same with all technologies today which are presented to us as progress and something that makes life easier. They have been developed with the intention of making money and controlling us and will always carry that. No matter how many supposed benefits your smartphone brings you, those who get rich by collecting your data and monitoring you will always benefit more than you. 

If in the past it was said "knowledge is power", today it should be said "information is power". The more the rulers know about their flocks, the better they can dominate them - in this sense, technology as a whole is a powerful instrument of control to predict and thereby prevent people from coming together to attack what oppresses them.

These smartphones seem to require a bit more than just a little electricity... In our generation, which at least still knew a world without smartphones, there are maybe still some people who understand what I'm talking about, who still know what it's like to hold a discussion without looking at their cellphone every thirty seconds, to get lost and thus discover new places or to debate about something without immediately asking Google for the answer. But I don't want to go back to the past, even if it wouldn't be possible anyway, but the more technology penetrates our lives, the harder it becomes to destroy it. What if we are one of the last generations still able to stop this evolution of human beings becoming completely controlled robots? 

And what if at some point we could no longer reverse this development? Humanity has reached a historically new stage with technology. A stage where it is able to annihilate all human life (nuclear energy) or to modify it (genetic manipulation). This fact underlines once again the need to act today to destroy this society. To do this, we need to encounter other people and communicate our ideas. 

Isn't it obvious that it will have a long term impact if, instead of talking to each other, we only communicate in messages of no more than five sentences. Apparently not. First of all, our way of thinking influences our way of speaking, and the reverse is also true - our way of speaking and communicating influences our way of thinking. If we are only able to exchange the shortest and most concise messages possible, how can we talk about a totally different world? And if we can't even talk about a different world anymore, how can we reach for it?

Direct communication between autonomous individuals is the basis of any shared rebellion, it is the starting point of shared dreams and common struggles. Without unmediated communication, a struggle against this world and for freedom is impossible.

Therefore, let's get rid of the smartphones and meet face to face in an insurgency against this world! Let's become uncontrollable!

