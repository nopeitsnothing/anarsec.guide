# AnarSec: Tech Guides for Anarchists 

Built with [Zola](https://www.getzola.org/).

If you would like to suggest edits to a guide, you can either [contact us](https://www.anarsec.guide/contact/) or submit an issue/merge request on 0xacab - whatever is your preference.

We are also open to submitted guides - please get in touch with proposals.

Our PGP public key can be verified at a second location [here](https://0xacab.org/anarsec/anarsec.guide/-/blob/no-masters/static/anarsec.asc) - commit history should display "Initial commit". WayBack Machine of PGP key: [anarsec.guide](https://web.archive.org/web/20230619164601/https://www.anarsec.guide/anarsec.asc) / [0xacab.org](https://web.archive.org/web/20230619164309/https://0xacab.org/anarsec/anarsec.guide/-/blob/no-masters/static/anarsec.asc)
